<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Import_history extends import  {
   public  $Coluums = array('ลำดับ','รหัสพนักงาน','ปี','รางวัล','ลำดับ','คะแนน');

   public function __construct($dbname)
   {
        parent::__construct($dbname);
   }
   
   public function get_datatable(){

       	$this->CI->load->model('history_award_model','data_model');
        $result = $this->CI->data_model->get_datatables($this->database_new_name);
        $data = array();
        $no = $_POST['start'];
        foreach ($result as $item) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $item->sale_code;
            $row[] = $item->year;
            $row[] = $item->award_code;
            $row[] = $item->order;
            $row[] = $item->score;
            $row[] = date('d-m-Y' , strtotime($item->created_at));
            $data[] = $row;
        }
        $json= array("draw" => $_POST['draw'] , 
					"recordsTotal" => $this->CI->data_model->count_all($this->database_new_name) ,
					'recordsFiltered' => $this->CI->data_model->count_filtered($this->database_new_name),
					"data"=> $data );

        return $json;
 
   }

   public function read_data_excel($FileName){
        $inputFileName = "upload/" . $FileName ;  
		$result = $this->get_data_excel($inputFileName);

		$first_arr=$result[0];	
		if(isset($first_arr['SalesCode']) && isset($first_arr['Award_Code']) && isset($first_arr['Score']) ){
			
            $i = 0;
           	foreach ($result as $data) {
				$i++;
				
			    $day = date("Y-m-d H:i:s");
			    $codeIndex ="";
			    $year ="";
			    if(isset($data['year'])){
					$year = ( $data['year'] - 543 );
			    }
				
				$data_array  = array(
					'sale_code'=> $data['SalesCode'],
					'award_code'=> $data['Award_Code'],
					'score'=> $data['Score'],
					'year'=> $year ,
					'order'=> $data['order'],
					'created_at'=> $day ,
				);

				$query = $this->otherdb
					->where('sale_code',$data['SalesCode'])
					->where('award_code',$data['Award_Code'])
					->where('year',$year )
					->get('tb_history');

				if ($query->num_rows() > 0) {
					$this->otherdb->where('sale_code',$data['SalesCode'])
					->where('award_code',$data['Award_Code'])
					->where('year',$year )
					->update('tb_history', $data_array);
				}
				else
				{
					$this->otherdb->insert('tb_history', $data_array);
					$id = $this->otherdb->insert_id();
				}

				
			}


            return true;
        }
        else{

            return false;
        }
   }

   public function read_data_txt($FileName) {

		$in_charset = 'UTF-8';   // == 'windows-874'
		$out_charset = 'UTF-8';

		$opts = array(
		'http'=>array(
			'method'=>"GET",
			'header'=> implode("\r\n", array(
						'Content-type: text/plain; charset=' . $in_charset
						))
		)
		);

		$key =0;

		// echo "<pre>";
		// foreach($recrod as $key => $seller){
		$lines=array();
		$f=fopen("./upload/".$FileName,"r");
		$this->otherdb->trans_start();

		while(!feof($f)){
			 $line=fgets($f, 65535);
			 $line = $this->iso8859_11toUTF8($line);

			
			$data = explode(",",$line);
			if(count($data)==20 && $key > 0)
			{

			
			// echo $this->mb_detect_encoding($data[3]);			
			// echo $this->iso8859_11toUTF8($data[3]);
			//  echo  iconv('iso-8859-1', 'utf-8', $data[3]) ;

			

	
			$day = date("Y-m-d H:i:s");
			// echo $data[9] . "<br/>" ;
			// echo date('Y-m-d', strtotime($data[9]) ) ;
			$data[19]=preg_replace("/[\n\r]/","",$data[19]); 

			$resign_date= '';
			if(!empty($data[19])){
		    	$resign_date = date('Y-m-d', strtotime($data[19]));
			}

			$arr  = array(
				'dealer_code'=> $data[0],
				'branch_code'=> $data[1],
				'salesman_code'=> $data[2],
				'firstname'=> $data[3],
				'lastname'=> $data[4],
				'salesman_id'=> $data[5],
				'sex'=> $data[6],
				'married'=> $data[7],
				'status'=> $data[8],
				'birthday'=> date('Y-m-d', strtotime($data[9]) ),
				'start_date'=> date('Y-m-d', strtotime($data[10]) ),
				'ddms'=> $data[12],
				'position'=> $data[13],
				'fleetsales_code'=> date('Y-m-d', strtotime($data[14])),
				'fleetsales_position'=> $data[15],
				'educational'=> $data[16],
				'religion'=> $data[17],
				'approval_date'=> date('Y-m-d', strtotime($data[18]) ),
				'resign_date'=> $resign_date ,
				'updated_at'=> date('Y-m-d', strtotime($data[11]) ),
				'created_at'=> $day ,
			);


				$this->otherdb->where('salesman_code =', $data[2] );
				$query = $this->otherdb->get('tb_employee');
				if($query->num_rows())
				{
					$this->otherdb->where('salesman_code', $data[2]);
					$this->otherdb->update('tb_employee', $arr);
				}
				else
				{
					$this->otherdb->insert('tb_employee', $arr);
					$lastid = $this->otherdb->insert_id();
				}


			}
			

			$key ++;
		}
		$this->otherdb->trans_complete();

		fclose($f);
		

		// echo $key;
		// echo 'Plain    : ', iconv("UTF-8", "ISO-8859-1", $fileText), PHP_EOL;

	}



}