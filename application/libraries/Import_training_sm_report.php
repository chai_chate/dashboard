<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Import_training_sm_report extends import  {
   
   public  $Coluums = array('ลำดับ','รหัสผู้แทนจำหน่าย','รหัสสาขา','รหัสผู้ฝึกสอน','ชื่อ','นามสกุล','Q1', 'Q2' ,'Q3', 'Q4' ,'อัพเดท');
   
   public function __construct($dbname)
   {
        parent::__construct($dbname);
   }
   
   public function get_datatable(){

       	$this->CI->load->model('saleman_report_model','data_model');
        $result = $this->CI->data_model->get_datatables($this->database_new_name);
        $data = array();
        $no = $_POST['start'];
        foreach ($result as $customers) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $customers->dealer_code;
            $row[] = $customers->branch_code;
            $row[] = $customers->training_code;
            $row[] = $customers->firstname;
            $row[] = $customers->lastname;
            $row[] = $customers->q1;
            $row[] = $customers->q2;
            $row[] = $customers->q3;
            $row[] = $customers->q4;
            $row[] = date('d-m-Y' , strtotime($customers->created_at));
 
            $data[] = $row;
        }
        $json= array("draw" => $_POST['draw'] , 
					"recordsTotal" => $this->CI->data_model->count_all($this->database_new_name) ,
					'recordsFiltered' => $this->CI->data_model->count_filtered($this->database_new_name),
					"data"=> $data );

        return $json;
 
   }

   public function read_data_excel($FileName){
        $inputFileName = "upload/" . $FileName ;  
		$result = $this->get_data_excel($inputFileName);

		$first_arr=$result[0];	
		if(isset($first_arr['D_Code']) && isset($first_arr['T_Code']) ){

            $i = 0;
			foreach ($result as $data) {
				$i++;
			    $day = date("Y-m-d H:i:s");
				$data_array  = array(
					'dealer_code'=> $data['D_Code'],
					'branch_code'=> $data['B_Code'],
					'branch_name'=> $data['B_Name'],
					'training_code'=> $data['T_Code'],
					'firstname'=> $data['T_Name'],
					'lastname'=> $data['T_Surname'],
					'training_id'=> $data['T_ID'],
					'q1'=> $data['Q1'],
					'q2'=> $data['Q2'],
					'q3'=> $data['Q3'],
					'q4'=> $data['Q4'],
					'created_at'=> $day ,
				);

				$query = $this->otherdb
					->where('training_code',$data['T_Code'])
					->get('tb_sm_report');

				if ($query->num_rows() > 0) {
					$this->otherdb
					->where('training_code',$data['T_Code'])
					->update('tb_sm_report', $data_array);
				}
				else
				{
					$this->otherdb->insert('tb_sm_report', $data_array);
					$id = $this->otherdb->insert_id();
				}
			}
            return true;
        }
        else{

            return false;
        }
   }



}