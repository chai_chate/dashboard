<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Import_dealers extends import  {
   public  $Coluums = array('ลำดับ','รหัสตัวแทน','ชื่อตัวแทน','รหัสกลุ่ม','กลุ่ม','ภูมิภาค','อัพเดท');
   
   
   public function __construct($dbname)
   {
        parent::__construct($dbname);
   }
   
   public function get_datatable(){

       	$this->CI->load->model('dealers_model');
        $result = $this->CI->dealers_model->get_datatables($this->database_new_name);
        $data = array();
        $no = $_POST['start'];
        foreach ($result as $customers) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $customers->dealer_code;
            $row[] = $customers->dealer_name;
            $row[] = $customers->group_id;
            $row[] = $customers->group;
            $row[] = $customers->region_code;
            $row[] = date('d-m-Y' , strtotime($customers->created_at));

            $data[] = $row;
        }
        $json= array("draw" => $_POST['draw'] , 
					"recordsTotal" => $this->CI->dealers_model->count_all($this->database_new_name) ,
					'recordsFiltered' => $this->CI->dealers_model->count_filtered($this->database_new_name),
					"data"=> $data );

        return $json;
 
   }

   public function read_data_excel($FileName){
        $inputFileName = "upload/" . $FileName ;  
		$result = $this->get_data_excel($inputFileName);

		$first_arr=$result[0];	
		if(isset($first_arr['Dlr_code']) && isset($first_arr['Group']) && isset($first_arr['Dlr_name']) ){
			
            $i = 0;
            foreach ($result as $data) {
                    $i++;
                    
                    $day = date("Y-m-d H:i:s");
                
                if(!isset($data['Code'])) {
                    $data['Code']= '';
                }
                    
                    $data_array  = array(
                        'dealer_code'=> $data['Dlr_code'],
                        'group'=> $data['Group'],
                        'dealer_name'=> $data['Dlr_name'],
                        'region_code'=> $data['Region'],
                        'group_id'=> $data['Code'],
                        'created_at'=> $day ,
                    );

                    $query = $this->otherdb
                        ->where('dealer_code',$data['Dlr_code'])
                        ->get('tb_dealers');

                    if ($query->num_rows() > 0) {
                        $this->otherdb
                        ->where('dealer_code',$data['Dlr_code'])
                        ->update('tb_dealers', $data_array);
                    }
                    else
                    {
                        $this->otherdb->insert('tb_dealers', $data_array);
                        $id = $this->otherdb->insert_id();
                    }
            }

            return true;
        }
        else{

            return false;
        }
   }



}