<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Import_license extends import  {
   public  $Coluums = array('ลำดับ','ใบอนูญาต','รหัสพนักงาน','attendance','ผลลัพธ์','อัพเดท');

   public function __construct($dbname)
   {
        parent::__construct($dbname);
   }
   
   public function get_datatable(){

       	$this->CI->load->model('license_model','data_model');
        $result = $this->CI->data_model->get_datatables($this->database_new_name);
        $data = array();
        $no = $_POST['start'];
        foreach ($result as $item) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $item->cl_code;
            $row[] = $item->sale_code;
            $row[] = $item->attendance;
            $row[] = $item->result;
            $row[] = date('d-m-Y' , strtotime($item->created_at));
            $data[] = $row;
        }
        $json= array("draw" => $_POST['draw'] , 
					"recordsTotal" => $this->CI->data_model->count_all($this->database_new_name) ,
					'recordsFiltered' => $this->CI->data_model->count_filtered($this->database_new_name),
					"data"=> $data );

        return $json;
 
   }

   public function read_data_excel($FileName){
        $inputFileName = "upload/" . $FileName ;  
		$result = $this->get_data_excel($inputFileName);

		$first_arr=$result[0];	
		if(isset($first_arr['Cl_Code']) && isset($first_arr['S_Code']) && isset($first_arr['Attendance']) ){
			
            $i = 0;
           	foreach ($result as $data) {
				$i++;
				
			    $day = date("Y-m-d H:i:s");
			    $codeIndex ="";
			    $year ="";
			    if(isset($data['Cl_Code'])){
					$code = substr($data['Cl_Code'],0,3) ;
			    }
				
				$data_array  = array(
					'sale_code'=> $data['S_Code'],
					'sale_id'=> $data['S_ID'],
					'cl_code'=> $data['Cl_Code'],
					'code'=> $code,
					'attendance'=> $data['Attendance'] ,
					'result'=> $data['TR_Result'] ,
					'created_at'=> $day ,
				);

				$query = $this->otherdb
					->where('sale_code',$data['S_Code'])
					->where('cl_code',$data['Cl_Code'])
					->get('tb_license');

				if ($query->num_rows() > 0) {
					$this->otherdb->where('sale_code',$data['S_Code'])
					->where('cl_code',$data['Cl_Code'])
					->update('tb_license', $data_array);
				}
				else
				{
					$this->otherdb->insert('tb_license', $data_array);
					$id = $this->otherdb->insert_id();
				}

				
			}

            return true;
        }
        else{

            return false;
        }
   }



}