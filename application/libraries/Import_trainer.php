<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Import_trainer extends import  {
   public  $Coluums = array('ลำดับ','รหัสสาขา','รหัสตัวแทน','รหัสผู้ฝึกสอน','ชื่อ','นามสกุล','ปีที่สมัคร','อัพเดท');
   
   public function __construct($dbname)
   {
        parent::__construct($dbname);
   }
   
   public function get_datatable(){

		$this->CI->load->model('training_model','data_model');
        $result = $this->CI->data_model->get_datatables($this->database_new_name);
        $data = array();
        $no = $_POST['start'];
		foreach ($result as $customers) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $customers->dealer_code;
            $row[] = $customers->dealer_name;
            $row[] = $customers->trainer_code;
            $row[] = $customers->firstname;
            $row[] = $customers->lastname;
            $row[] = $customers->apply_year;
            $row[] = date('d-m-Y' , strtotime($customers->created_at));
 
            $data[] = $row;
        }
        
        $json= array("draw" => $_POST['draw'] , 
					"recordsTotal" => $this->CI->data_model->count_all($this->database_new_name) ,
					'recordsFiltered' => $this->CI->data_model->count_filtered($this->database_new_name),
					"data"=> $data );

        return $json;
 
   }

   public function read_data_excel($FileName){
        $inputFileName = "upload/" . $FileName ;  
		$result = $this->get_data_excel($inputFileName);

		$first_arr=$result[0];	
		if(isset($first_arr['Dlr_Code']) && isset($first_arr['Br_Code']) && isset($first_arr['Tn_Code']) ){

			
            $i = 0;
            foreach ($result as $data) {
				$i++;
				
			    $day = date("Y-m-d H:i:s");
			   
			   if(!isset($data['Tn_Code'])) {
				   $data['Tn_Code']= '';
			   }
			   $year = '';
			   if(isset($data['ApplyForY'])){
					$year = $data['ApplyForY']-543 ; ;
			    }
				
				$data_array  = array(
					'dealer_code'=> $data['Dlr_Code'],
					'dealer_name'=> $data['DLR_NAME'],
					'branch_code'=> $data['Br_Code'],
					'trainer_id'=> $data['T_ID'],
					'trainer_code'=> $data['Tn_Code'],
					'firstname'=> $data['T_Name'],
					'lastname'=> $data['Surname'],
					'apply'=> $data['Apply'] ,
					'apply_year'=> $year ,
					'rindex'=> $data['Rindex'],
					'created_at'=> $day ,
				);

				$query = $this->otherdb
					->where('trainer_code',$data['Tn_Code'])
					->get('tb_trainer');

				if ($query->num_rows() > 0) {
					$this->otherdb
					->where('trainer_code',$data['Tn_Code'])
					->update('tb_trainer', $data_array);
				}
				else
				{
					$this->otherdb->insert('tb_trainer', $data_array);
					$id = $this->otherdb->insert_id();
				}
		    }

            return true;
        }
        else{

            return false;
        }
   }



}