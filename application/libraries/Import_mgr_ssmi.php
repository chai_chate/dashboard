<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Import_mgr_ssmi extends import  {

   public  $Coluums = array('ลำดับ','รหัสตัวแทน','รหัสสาขา','ค่าเฉลี่ย','เดือน','ปี','อัพเดท');
   
   
   public function __construct($dbname)
   {
        parent::__construct($dbname);
   }
   
   public function get_datatable(){

       	$this->CI->load->model('mgr_ssmi_model','data_model');
        $result = $this->CI->data_model->get_datatables($this->database_new_name);
        
		$data = array();
        $no = $_POST['start'];
        foreach ($result as $item) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $item->dealer_code;
            $row[] = $item->branch_code;
            $row[] = $item->avg;
            $row[] = $item->month;
            $row[] = $item->year;
            $row[] = date('d-m-Y' , strtotime($item->created_at));
            $data[] = $row;
        }
 
        $json= array("draw" => $_POST['draw'] , 
					"recordsTotal" => $this->CI->data_model->count_all($this->database_new_name) ,
					'recordsFiltered' => $this->CI->data_model->count_filtered($this->database_new_name),
					"data"=> $data );

        return $json;
 
   }

   public function read_data_excel($FileName){
        $inputFileName = "upload/" . $FileName ;  
		$result = $this->get_data_excel($inputFileName);

		$first_arr=$result[0];	
		if(isset($first_arr['Month']) && isset($first_arr['Dlr_Code']) ){
			
            $i = 0;
            foreach ($result as $data) {
				$i++;
			    $day = date("Y-m-d H:i:s");
				$year = '';
			    $month ='';
				if(isset($data['Month'])){
					$arr= explode("-",$data['Month']);
					$month = (int)$arr[0];
					$year = "20" .($arr[1]-43);
			    }

				$data_array  = array(
					'month'=> $month ,
					'year'=> $year ,
					'dealer_code'=> $data['Dlr_Code'] ,
					'branch_code'=> $data['Br'] ,
					'initiation'=> $data['Sales Initiation'] ,
					'salesperson'=> $data['Salesperson'] ,
					'delivery'=> $data['Delivery Process'] ,
					'avg'=> $data['Avg'] ,
					'remark'=> $data['Remark'] ,
					'created_at'=> $day ,
				);

				$query = $this->otherdb
					->where('month',$month)
					->where('year', $year )
					->where('dealer_code',$data['Dlr_Code'])
					->where('branch_code',$data['Br'])
					->get('tb_mgr_ssmi');

				if ($query->num_rows() > 0) {
					$this->otherdb
					->where('month',$month)
					->where('year', $year )
					->where('dealer_code',$data['Dlr_Code'])
					->where('branch_code',$data['Br'])
					->update('tb_mgr_ssmi', $data_array);
				}
				else
				{
					$this->otherdb->insert('tb_mgr_ssmi', $data_array);
					$id = $this->otherdb->insert_id();
				}
			}

            return true;
        }
        else{

            return false;
        }
   }



}