<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Import_trainer_radar extends import  {
   public  $Coluums = array('ลำดับ','ไตรมาส','ปี','รหัสผู้ฝึกสอน','ที่มา', 'ประเภท' ,'วันที่พบ', 'ตรวจสอบ' ,'อัพเดท');
   
   
   public function __construct($dbname)
   {
        parent::__construct($dbname);
       	$this->CI->load->model('Trainer_radar_model','data_model');
   }
   
   public function get_datatable(){

        $result = $this->CI->data_model->get_datatables($this->database_new_name);
        $data = array();
        $no = $_POST['start'];
        foreach ($result as $customers) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $customers->quarter;
            $row[] = $customers->year;
            $row[] = $customers->trainer_code;
            $row[] = $customers->sorce_data;
            $row[] = $customers->type;
            $row[] = $customers->issue_date;
            $row[] = $customers->completed_check;
            $row[] = date('d-m-Y' , strtotime($customers->created_at));
 
            $data[] = $row;
        }
        $json= array("draw" => $_POST['draw'] , 
					"recordsTotal" => $this->CI->data_model->count_all($this->database_new_name) ,
					'recordsFiltered' => $this->CI->data_model->count_filtered($this->database_new_name),
					"data"=> $data );

        return $json;
 
   }

   public function read_data_excel($FileName){
        $inputFileName = "upload/" . $FileName ;  
		$result = $this->get_data_excel($inputFileName);

		$first_arr=$result[0];	
		if(isset($first_arr['Quarter']) && isset($first_arr['T_Code']) ){
			
            $i = 0;
            foreach ($result as $data) {
				$i++;
			    $day = date("Y-m-d H:i:s");

				$year = '';
				$quarter ='';
				if(isset($data['Quarter'])){
					$arr= explode("-",$data['Quarter']);
					$quarter = (int)$arr[0];
					$year = "20" .($arr[1]-43);
				}

				// var_dump($data['Quarter']);
				// exit();


				$data_array  = array(
					'quarter'=> $quarter ,
					'year'=> $year ,
					'trainer_code'=> $data['T_Code'],
					'report_no'=> $data['Report_No'],
					'issue_date'=> $this->dateExcel2date($data['Issue_Date']),
					'type'=> $data['Type'],
					'sorce_data'=> $data['Sorce_data'],
					'recording_date'=> $this->dateExcel2date($data['Recording_date']),
					'completed_check'=> $data['Completed_Check'],
					'report_index'=> $data['Report_Index'],
					'created_at'=> $day ,
				);

				$query = $this->otherdb
					->where('report_index',$data['Report_Index'])
					->get('tb_trainer_radar');

				if ($query->num_rows() > 0) {
					$this->otherdb
					->where('report_index',$data['Report_Index'])
					->update('tb_trainer_radar', $data_array);
				}
				else
				{
					$this->otherdb->insert('tb_trainer_radar', $data_array);
					$id = $this->otherdb->insert_id();
				}
			}

            return true;
        }
        else{

            return false;
        }
   }



}