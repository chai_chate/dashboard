<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Import_mgr_showroom extends import  {

   public  $Coluums = array('ลำดับ','รหัสตัวแทน','ชื่อตัวแทน','รหัสสาขา','กลุ่ม','คะแนน','อัพเดท');
   
   
   public function __construct($dbname)
   {
        parent::__construct($dbname);
   }
   
   public function get_datatable(){

       	$this->CI->load->model('mgr_showroom_model','data_model');
        $result = $this->CI->data_model->get_datatables($this->database_new_name);
        
		$data = array();
        $no = $_POST['start'];
        foreach ($result as $customers) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $customers->dealer_code;
            $row[] = $customers->dealer_name;
            $row[] = $customers->branch_code;
            $row[] = $customers->group;
            $row[] = $customers->grade;
            $row[] = date('d-m-Y' , strtotime($customers->created_at));
 
            $data[] = $row;
        }
 
        $json= array("draw" => $_POST['draw'] , 
					"recordsTotal" => $this->CI->data_model->count_all($this->database_new_name) ,
					'recordsFiltered' => $this->CI->data_model->count_filtered($this->database_new_name),
					"data"=> $data );

        return $json;
 
   }

   public function read_data_excel($FileName){
        $inputFileName = "upload/" . $FileName ;  
		$result = $this->get_data_excel($inputFileName);

		$first_arr=$result[0];	
		if(isset($first_arr['Dlr_Code']) && isset($first_arr['Grade']) ){
			
            $i = 0;
            foreach ($result as $data) {
				$i++;
			    $day = date("Y-m-d H:i:s");

				$data_array  = array(
					'group'=> $data['Group'] ,
					'dealer_code'=> $data['Dlr_Code'] ,
					'dealer_name'=> $data['Dlr_Name'] ,
					'branch_code'=> $data['Br_Code'] ,
					'location_name'=> $data['Location_Name'] ,
					'grade'=> $data['Grade'] ,
					'created_at'=> $day ,
				);

				$query = $this->otherdb
					->where('branch_code',$data['Br_Code'])
					->where('dealer_code',$data['Dlr_Code'])
					->get('tb_mgr_manage_showroom');

				if ($query->num_rows() > 0) {
					$this->otherdb
					->where('branch_code',$data['Br_Code'])
					->where('dealer_code',$data['Dlr_Code'])
					->update('tb_mgr_manage_showroom', $data_array);
				}
				else
				{
					$this->otherdb->insert('tb_mgr_manage_showroom', $data_array);
					$id = $this->otherdb->insert_id();
				}



			}

            return true;
        }
        else{

            return false;
        }
   }



}