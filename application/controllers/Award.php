<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Award extends MY_Controller {

    var $page_level_css = array(
        "assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" ,
        "assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css" ,
        "assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css",
        "assets/plugins/bootstrap-datepicker/css/datepicker.css",
        "assets/plugins/bootstrap-datepicker/css/datepicker3.css",
    );

	var $page_level_js = array(
            'assets/plugins/DataTables/media/js/jquery.dataTables.js',
            'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js' ,
            "assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js",
            "assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js",
            "assets/plugins/bootstrap-daterangepicker/moment.js",
            "assets/plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js",
            "assets/js/table-manage-buttons.demo.js",
            "assets/js/form-schedule.js",
		);


	public function __construct(){
		parent::__construct();

		$this->load->model('schedule_model');
		$this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

	}

    public function page($id = null){
        $search_arr = array();

        $this->middle = 'award/award';
        $this->title = 'ຖືກລາງວັນ';
        $this->js = array('FormPlugins.init();', 'TableManageButtons.init();');
        // $this->data['search'] = $search_arr ;
        if($id){
            $this->data['current'] = $this->schedule_model->get_schedulebyId($id);
        }
        $this->data['schedule'] = $this->schedule_model->get_schedule();
        $this->data['breadcrumb'] = array(
                array('name'=>'ຕາຕະລາງຖືກລາງວັນ' , 'link' => BASE_URL('salevolume') , 'active' => false ) ,
                // array('name'=>'เบอร์ถูกรางวัล' , 'link' => BASE_URL("salevolume/table") , 'active' => true )
            ); 

        $this->view();
    }

	public function index()
	{
       $this->page();
	}
    public function save_award() {

        $day_id = $this->input->post('day');
        $number = $this->input->post('number');
       

        $arr = array(
            'lottery_number' =>  $number ,
            'updated_at'=> date('Y-m-d'),
        );
        $this->schedule_model->update_schedule_number($arr , $day_id);

        echo "<script>
                alert('บันทึกข้อมูลเรียบร้อยแล้ว');
                window.location.href='/award/page/{$day_id}';
             </script>";
    }

    public function update_status($status , $id=0)
    {
        $result =$this->schedule_model->update_status($status,$id);

        echo "<script>
            alert('บันทึกข้อมูลเรียบร้อยแล้ว');
            window.location.href='/schedule';
        </script>";
    }



}
