<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 300000); //300 seconds = 5 minutes30
ini_set("memory_limit","2024M");//128

class Adjust extends MY_Controller {
	var $page_level_css = array(
		"assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" ,
        "assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css" ,
        "assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css",
		"assets/plugins/parsley/src/parsley.css",
		"assets/plugins/jquery-file-upload/css/jquery.fileupload.css" ,
		"assets/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" ,
		"assets/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css" ,
        "assets/plugins/gritter/css/jquery.gritter.css"
	);

	var $page_level_js = array(
		    'assets/plugins/DataTables/media/js/jquery.dataTables.js',
            'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js' ,
            "assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js",
            "assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js",
		    "assets/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js",
            "assets/plugins/jquery-file-upload/js/vendor/load-image.min.js",
            "assets/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js",
            "assets/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js",
            "assets/plugins/jquery-file-upload/js/jquery.iframe-transport.js",
            "assets/plugins/jquery-file-upload/js/jquery.fileupload.js",
            "assets/plugins/jquery-file-upload/js/jquery.fileupload-process.js",
            "assets/plugins/jquery-file-upload/js/jquery.fileupload-image.js",
            "assets/plugins/jquery-file-upload/js/jquery.fileupload-audio.js",
            "assets/plugins/jquery-file-upload/js/jquery.fileupload-video.js",
            "assets/plugins/jquery-file-upload/js/jquery.fileupload-validate.js",
            "assets/plugins/jquery-file-upload/js/jquery.fileupload-ui.js",
            "assets/plugins/gritter/js/jquery.gritter.js",
            "assets/js/award-adjust.js",
			"assets/plugins/parsley/dist/parsley.js"
		);


	var $dbname_default = "saleaward_";
	var $otherdb ;
	var $myforge ;
	public function __construct(){
		parent::__construct();
		
		// echo phpinfo();
		// exit();
		
		$this->config->load('config_table');
		$this->load->dbutil();
		$this->load->dbforge();
		$this->load->model('adjust_model');
		$this->load->model('user_model');
		$this->load->model('dealers_model');
		$this->load->model('employee_model');
		$this->load->model('training_model');
		$this->load->model('global_model');

		$this->load->library('form_validation');
		$this->load->library('upload');
		// $this->load->dbforge($otherdb);
	}


	public function createDatabase(){
		
		// var_dump($_POST);
		$name = $_POST['year'];
		$arr = array();
		$database_name = $this->dbname_default . $name ;
		if($name){
			if (!$this->dbutil->database_exists($database_name))
			{
				if ($this->dbforge->create_database($database_name))
				{

					$arr = array(
						'year'=> $name ,
						'db_name'=> $database_name ,
						'created_at'=> date('Y-m-d H:i:s')
					);
					$this->adjust_model->set_year($arr);
					$this->create_table($database_name);
					$arr = array("status" => true , "message" =>"สร้างฐานข้อมูลเรียบร้อยแล้ว!") ;
				}
				else{
					$arr = array("status" => false , "message" =>"เกิดเหตุขัดข้องบางประการ ไม่สามารถสร้างฐานข้อมูลได้") ;
				}
			}
			else{
				$arr = array("status" => false , "message" =>"มีฐานข้อมูลนี้แล้ว ไม่สามารถสร้างฐานข้อมูลได้") ;
			}
		}
		else
		{
			$arr = array("status" => false , "message" =>"กรุณาระบุปีฐานข้อมูลที่สร้าง") ;
		}
		
		$json =json_encode($arr);


		echo  $json ; 

	}

	public function create_table($database_new_name){
		$otherdb =$this->load->database("otherdb", TRUE);
		$otherdb->db_select($database_new_name);
		$this->load->dbforge($otherdb);

	
	    $table_settings = $this->config->item('table_settings');
		foreach($table_settings as $tablename => $item){
			$this->dbforge->add_field('id', true );
			$this->dbforge->add_field($item['fields']);

			if($item['keys']){

				foreach($item['keys'] as $key => $keys){
					
					if(is_array($keys)){
						$this->dbforge->add_key($keys);
					}
					else
					{
						$this->dbforge->add_key($keys);
					}

					
					unset($keys);
				}
			}
			$attributes = array('ENGINE' => 'InnoDB');
			$this->dbforge->create_table( $tablename , true , $attributes);
		}

	}

	public function delDatabase(){
		$id = $_POST['id'];
		$password = $_POST['password'];
		$arr = array();

		 $user = $this->session->userdata("logged_in");
		 $count_user =$this->user_model->get_UserPassDuplicate($user['id'],$password);
		 
		//  var_dump($count_user);
		$database_name = '';
		if($count_user)
		{
			$result =$this->adjust_model->get_year_by_id($id);
			if($result)
			{
				$database_name = $result['db_name'];
			}

			if ($this->dbutil->database_exists($database_name))
			{
				$result =$this->adjust_model->del_year($id);
				if ($this->dbforge->drop_database($database_name))
				{
					$arr = array("status"=> true , "message"=> "ลบฐานข้อมูลเรียบร้อยแล้ว");
				}
			}
			else
			{
				$arr = array("status"=> false , "message"=> "ไม่มีฐานข้อมูลนี้");
			}

		}
		else
		{	
			$arr = array("status"=> false , "message"=> "รหัสผ่านไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง");
		}
		
		echo json_encode($arr);

	}
	
	public function index()
	{
		
		$this->middle = 'dealer/index';
		$this->title = 'ฐานข้อมูลผู้แทนจำหน่าย';
	
		$this->data['breadcrumb'] = array(
			array('name'=>'การปรับปรุงฐานข้อมูล' , 'link' => BASE_URL('dealer') , 'active' => false ) ,
			array('name'=>'ฐานข้อมูลผู้แทนจำหน่าย' , 'link' => BASE_URL("dealer") , 'active' => true )
		); 
        $this->js = array('FormPlugins.init();', 'TableManageButtons.init();');
		$this->view();
	}

	public function iso8859_11toUTF8($string) {
	
		if (preg_match("[\241-\377]", $string)) 
		return $string;

		$iso8859_11 = array(
			"\xa1" => "\xe0\xb8\x81",
			"\xa2" => "\xe0\xb8\x82",
			"\xa3" => "\xe0\xb8\x83",
			"\xa4" => "\xe0\xb8\x84",
			"\xa5" => "\xe0\xb8\x85",
			"\xa6" => "\xe0\xb8\x86",
			"\xa7" => "\xe0\xb8\x87",
			"\xa8" => "\xe0\xb8\x88",
			"\xa9" => "\xe0\xb8\x89",
			"\xaa" => "\xe0\xb8\x8a",
			"\xab" => "\xe0\xb8\x8b",
			"\xac" => "\xe0\xb8\x8c",
			"\xad" => "\xe0\xb8\x8d",
			"\xae" => "\xe0\xb8\x8e",
			"\xaf" => "\xe0\xb8\x8f",
			"\xb0" => "\xe0\xb8\x90",
			"\xb1" => "\xe0\xb8\x91",
			"\xb2" => "\xe0\xb8\x92",
			"\xb3" => "\xe0\xb8\x93",
			"\xb4" => "\xe0\xb8\x94",
			"\xb5" => "\xe0\xb8\x95",
			"\xb6" => "\xe0\xb8\x96",
			"\xb7" => "\xe0\xb8\x97",
			"\xb8" => "\xe0\xb8\x98",
			"\xb9" => "\xe0\xb8\x99",
			"\xba" => "\xe0\xb8\x9a",
			"\xbb" => "\xe0\xb8\x9b",
			"\xbc" => "\xe0\xb8\x9c",
			"\xbd" => "\xe0\xb8\x9d",
			"\xbe" => "\xe0\xb8\x9e",
			"\xbf" => "\xe0\xb8\x9f",
			"\xc0" => "\xe0\xb8\xa0",
			"\xc1" => "\xe0\xb8\xa1",
			"\xc2" => "\xe0\xb8\xa2",
			"\xc3" => "\xe0\xb8\xa3",
			"\xc4" => "\xe0\xb8\xa4",
			"\xc5" => "\xe0\xb8\xa5",
			"\xc6" => "\xe0\xb8\xa6",
			"\xc7" => "\xe0\xb8\xa7",
			"\xc8" => "\xe0\xb8\xa8",
			"\xc9" => "\xe0\xb8\xa9",
			"\xca" => "\xe0\xb8\xaa",
			"\xcb" => "\xe0\xb8\xab",
			"\xcc" => "\xe0\xb8\xac",
			"\xcd" => "\xe0\xb8\xad",
			"\xce" => "\xe0\xb8\xae",
			"\xcf" => "\xe0\xb8\xaf",
			"\xd0" => "\xe0\xb8\xb0",
			"\xd1" => "\xe0\xb8\xb1",
			"\xd2" => "\xe0\xb8\xb2",
			"\xd3" => "\xe0\xb8\xb3",
			"\xd4" => "\xe0\xb8\xb4",
			"\xd5" => "\xe0\xb8\xb5",
			"\xd6" => "\xe0\xb8\xb6",
			"\xd7" => "\xe0\xb8\xb7",
			"\xd8" => "\xe0\xb8\xb8",
			"\xd9" => "\xe0\xb8\xb9",
			"\xda" => "\xe0\xb8\xba",
			"\xdf" => "\xe0\xb8\xbf",
			"\xe0" => "\xe0\xb9\x80",
			"\xe1" => "\xe0\xb9\x81",
			"\xe2" => "\xe0\xb9\x82",
			"\xe3" => "\xe0\xb9\x83",
			"\xe4" => "\xe0\xb9\x84",
			"\xe5" => "\xe0\xb9\x85",
			"\xe6" => "\xe0\xb9\x86",
			"\xe7" => "\xe0\xb9\x87",
			"\xe8" => "\xe0\xb9\x88",
			"\xe9" => "\xe0\xb9\x89",
			"\xea" => "\xe0\xb9\x8a",
			"\xeb" => "\xe0\xb9\x8b",
			"\xec" => "\xe0\xb9\x8c",
			"\xed" => "\xe0\xb9\x8d",
			"\xee" => "\xe0\xb9\x8e",
			"\xef" => "\xe0\xb9\x8f",
			"\xf0" => "\xe0\xb9\x90",
			"\xf1" => "\xe0\xb9\x91",
			"\xf2" => "\xe0\xb9\x92",
			"\xf3" => "\xe0\xb9\x93",
			"\xf4" => "\xe0\xb9\x94",
			"\xf5" => "\xe0\xb9\x95",
			"\xf6" => "\xe0\xb9\x96",
			"\xf7" => "\xe0\xb9\x97",
			"\xf8" => "\xe0\xb9\x98",
			"\xf9" => "\xe0\xb9\x99",
			"\xfa" => "\xe0\xb9\x9a",
			"\xfb" => "\xe0\xb9\x9b"
			);

		$string=strtr($string,$iso8859_11);
		return $string;
	}

	public function add_competition()
	{
		
		$this->middle = 'adjust/add_competition';
		$this->title = 'สร้างการแข่งขันประจำปี';

		$this->data['years'] = $this->adjust_model->get_year();
	
		$this->data['breadcrumb'] = array(
			array('name'=>'การปรับปรุงฐานข้อมูล' , 'link' => BASE_URL('dealer') , 'active' => false ) ,
			array('name'=>'สร้างการแข่งขันประจำปี' , 'link' => BASE_URL("dealer") , 'active' => true )
		); 
        $this->js = array('Adjust.init();');
		$this->view();
	}

	public function get_level($id){
		$obj_import=$this->global_model->get_data("tb_import",array("level_id"=>$id),array("sort"=>"asc"));
		echo json_encode($obj_import);
	}

	public function import($type,$year=null)
	{
		
		$obj_import=$this->global_model->get_data_by_id("tb_import",$type);

		$this->middle = 'adjust/import';
		$this->data['years'] = $this->adjust_model->get_year();
		$this->data['type'] = $type;
		$this->data['year'] = $year;


		$this->title = 'นำเข้า' . $obj_import->name;
		$this->titleTable = $obj_import->name .'ปี ' . $year;
		$this->data['url']= base_url(). 'adjust/get_datatable/' . $type ."/". $year;
		$obj = $this->call_class_import($type,$year);
		$this->data['columns']= $obj->Coluums;
		$this->data['level']= $obj_import->level_id;

		// $this->load->library('import');

	
		$this->data['breadcrumb'] = array(
			array('name'=>'การปรับปรุงฐานข้อมูล' , 'link' => BASE_URL('dealer') , 'active' => false ) ,
			array('name'=>'นำเข้าข้อมูล' , 'link' => BASE_URL("dealer") , 'active' => true )
		); 
        $this->js = array('Adjust.init();');
		$this->view();
	}

	protected function call_class_import($type,$year) {
		$obj_import=$this->global_model->get_data_by_id("tb_import",$type);
		$class_name=$obj_import->class_name;
		$database_new_name = $this->dbname_default. $year ;
		$params = array('database_name' => $database_new_name );
		$this->load->library('import',$params);
		$this->load->library($class_name,$params);
		$obj=$this->{$class_name};
		return $obj;
	}


	public function get_datatable($type,$year){

		$json = array();
		$obj = $this->call_class_import($type,$year);
		$json= $obj->get_datatable();

		echo json_encode($json);
	}

	public function alert_result($result){
		$this->middle = 'global/alert/alert-result';
        $this->title = 'เกิดข้อผิดพลาดบางประการ';
		// var_dump($result);
		// exit();
		$this->data['result'] = urldecode($result) ;
        $this->view();
	}

	public function do_upload(){

		// $fileName  = $_POST['filename'];
		$year  = $_POST['year'];
		$type  = $_POST['type'];

        $config['upload_path']          = './upload';
        $config['allowed_types']        = 'xls|xlsx|XLS|XLSX|csv|txt';
        $config['encrypt_name'] = TRUE;
		$config['max_size']             = 20480;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        // $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload('files'))
        {
		    $alert = $this->upload->display_errors();

			print_r($alert);
			// redirect('/Adjust/alert_result/' . $alert , 'refresh');
			
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
			// print_r( $data['upload_data']);
			// exit();
			$fileName  = $data['upload_data']['file_name'] ;
			$obj = $this->call_class_import($type,$year);

			// echo "<pre>";
			// print_r($obj);

			if($data['upload_data']['file_ext'] === '.txt'){
				$result =$obj->read_data_txt($fileName);
			}
			else
			{
				$result = $obj->read_data_excel($fileName);
			}

            $path =$data['upload_data']['full_path'];
			if(is_file($path)){
				unlink($path); // delete file
			}

			if( !$result ){
				$alert = "นำเข้าข้อมูลไม่ถูกต้อง";
				// print_r($alert);
				// redirect('/Adjust/alert_result/' . $alert , 'refresh');
			}

        }

		redirect("/Adjust/import/{$type}/{$year}", 'refresh');
    }

	





}
