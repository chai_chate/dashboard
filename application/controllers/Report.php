<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends MY_Controller {
    var $db2 ;
    var $page_level_js = array(
		'assets/plugins/parsley/dist/parsley.js',
		'assets/js/award-report.js',
	);
	public function __construct(){
		parent::__construct();
        $this->load->model('employee_model');
        $this->load->model('award_model');

	}

    public function index(){
        
    }

    public function nation(){
        $this->middle = 'report/index';
		$this->title = 'การรายงานผล-ตามประเภทรางวัล';


		$this->data['employee_level'] = $this->employee_model->get_employee_level();
		$this->data['award_year'] = $this->award_model->get_award_year();

		
         $this->data['breadcrumb'] = array(
            array('name'=>'การรายงานผล' , 'link' => BASE_URL('report') , 'active' => false ) ,
            array('name'=>'การรายงานผลตามประเภทรางวัล' , 'link' => BASE_URL("report/nation") , 'active' => true )
        ); 
		$this->js = array('report.init();');
		$this->view();
    }
    public function get_option_award_type($id){
        $arr = array("employee_level_id"=>$id);
        $result =  $this->award_model->get_award_type($arr);
        $html = "";
        if(is_array($result)){
            foreach($result as $key => $item){
                $html .=  "<option value='{$item->id}'>{$item->name_th}</option>";
            }
        }


        echo $html ;

    }

    public function get_award($type){

         $where = array(
          'type' => $type,
        );
         $this->db->order_by('score', 'desc'); 
        //  $this->db->order_by('order',null); 

        $query = $this->db->get_where('tb_award', $where );
        // echo $this->db->last_query();
        $result = array();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }

        return $result ;
   }


   public function create(){

        if(!isset($_POST['award_type'])){
            redirect('/report/nation', 'refresh');
        }
        $award_type_id = $_POST['award_type'];

        $year = $_POST['year'];
        $where = array(
          'type' => $award_type_id,
        );
    

        $result= $this->award_model->get_award_type_by_id($award_type_id , $year);
        $reportData = $this->award_model->get_award_list($where , $result['limit'] , $year );
        $filename = $result['name_th'] . "_{$year}.xlsx";


        // var_dump($result);
       
        $this->excel_create($filename,$reportData);
   }

   protected function excel_create($filename,$reportData){

        $file_path = FCPATH.'assets/file/template.xlsx';
        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load($file_path);
        $objSheet = $objPHPExcel->setActiveSheetIndex(0);
        
        foreach($reportData as $key => $item){
            //var_dump($item);
            //exit();
            $objSheet->setCellValue('A'.($key+3), ($key+1));
            $objSheet->setCellValue('B'.($key+3), $item['name'] );
            $objSheet->setCellValue('C'.($key+3), $item['lastname'] );
            $objSheet->setCellValue('D'.($key+3), $item['sale_code'] );
            $objSheet->setCellValue('E'.($key+3), $item['branch_code'] );
            $objSheet->setCellValue('F'.($key+3), $item['dealer_name'] );
            $objSheet->setCellValue('G'.($key+3), $item['monthly_score'] );
            $objSheet->setCellValue('H'.($key+3), $item['regularity_score'] );
            $objSheet->setCellValue('I'.($key+3), $item['exam_score'] );
            $objSheet->setCellValue('J'.($key+3), $item['interview_score'] );
            $objSheet->setCellValue('K'.($key+3), $item['ssmi_score'] );
            $objSheet->setCellValue('L'.($key+3), $item['total_score'] );
            
        }
        // $objSheet->setCellValue('G19', $totalPrice);
        
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); //.xlsx
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
   }


    public function export($type){

        $arr = array("", "TSC","TSE","TSE-TSG");
        $nameType = $arr[$type] ;


        $reportData = $this->get_award($type);
        // var_dump($reportData);
        $filename = "รายงานพนักงานขาย {$nameType}.xlsx";

        $file_path = FCPATH.'assets/file/template.xlsx';
        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load($file_path);
        $objSheet = $objPHPExcel->setActiveSheetIndex(0);
        // $objSheet->setCellValue('C5', $scheduleData->id);
        // $objSheet->setCellValue('F5', $scheduleData->day);
        // echo  "<pre>";
        foreach($reportData as $key => $item){
            //var_dump($item);
            //exit();
            $objSheet->setCellValue('A'.($key+3), ($key+1));
            $objSheet->setCellValue('B'.($key+3), $item['name'] );
            $objSheet->setCellValue('C'.($key+3), $item['lastname'] );
            $objSheet->setCellValue('D'.($key+3), $item['sale_code'] );
            $objSheet->setCellValue('E'.($key+3), $item['branch_code'] );
            $objSheet->setCellValue('F'.($key+3), $item['dealer_name'] );
            $objSheet->setCellValue('G'.($key+3), $item['point1'] );
            $objSheet->setCellValue('H'.($key+3), $item['point1_2'] );
            $objSheet->setCellValue('I'.($key+3), $item['point2'] );
            $objSheet->setCellValue('J'.($key+3), $item['point2_2'] );
            $objSheet->setCellValue('K'.($key+3), $item['point3'] );
            $objSheet->setCellValue('L'.($key+3), $item['score'] );
            
        }
        // $objSheet->setCellValue('G19', $totalPrice);
        
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); //.xlsx
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    
        
    }
	
}