<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	var $page_level_js = array(
		'assets/plugins/morris/raphael.min.js',
		'assets/plugins/morris/morris.js',
		'assets/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min.js' ,
		'assets/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js' ,
		'assets/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js',
		'assets/plugins/gritter/js/jquery.gritter.js',
		'assets/js/dashboard-v2.min.js',
	);

	public function __construct(){
		parent::__construct();
		
		// if (!$this->is_logged_in())
        // {
		// 	redirect('login', 'refresh');
        // }

		// $this->load->model('schedule_model');
		
	}

	public function index()
	{
		
		$this->middle = 'dashboard';
		$this->title = 'dashboard';
		// $this->load->model('global_model');
		$sql = 'SELECT *
				FROM tb_year
				WHERE year =  ( SELECT max(year) FROM tb_year )';
		$query = $this->db->query($sql);
        // $result =$query->result_array();
        $result = $query->row();
		
		if($result){
			$this->load->model('employee_model');
			$this->load->model('Dealers_model');

			$dbName = $result->db_name;

			$this->employee_model->_change_database($dbName);
			$this->Dealers_model->_change_database($dbName);

			$this->data['count_seller'] = $this->employee_model->get_employee_typeCount('01');
			$this->data['count_trainer'] = $this->employee_model->get_employee_typeCount('02');
			$this->data['count_mrg'] = $this->employee_model->get_employee_typeCount('05');
			$this->data['count_deales'] = $this->Dealers_model->get_Count();
		}
		// $this->data['schedule'] = $schedule_last;
		// $this->data['total_summary'] = $this->lotto_model->total_sale_summary($schedule_last->id);

		$this->js = array('DashboardV2.init();');
		$this->view();
	}
}
