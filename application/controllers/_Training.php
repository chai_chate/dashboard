<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('max_execution_time', 30000); //300 seconds = 5 minutes30
ini_set("memory_limit","2024M");//128

class Training extends MY_Controller {
	var $page_level_css = array(
		"assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" ,
        "assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css" ,
        "assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css",
		"assets/plugins/parsley/src/parsley.css",
		"assets/plugins/jquery-file-upload/css/jquery.fileupload.css" ,
		"assets/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" ,
		"assets/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css" ,
        "assets/plugins/gritter/css/jquery.gritter.css"
	);

	var $page_level_js = array(
		    'assets/plugins/DataTables/media/js/jquery.dataTables.js',
            'assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js' ,
            "assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/jszip.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js",
            "assets/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js",
            "assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js",
		    "assets/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js",
            "assets/plugins/jquery-file-upload/js/vendor/load-image.min.js",
            "assets/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js",
            "assets/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js",
            "assets/plugins/jquery-file-upload/js/jquery.iframe-transport.js",
            "assets/plugins/jquery-file-upload/js/jquery.fileupload.js",
            "assets/plugins/jquery-file-upload/js/jquery.fileupload-process.js",
            "assets/plugins/jquery-file-upload/js/jquery.fileupload-image.js",
            "assets/plugins/jquery-file-upload/js/jquery.fileupload-audio.js",
            "assets/plugins/jquery-file-upload/js/jquery.fileupload-video.js",
            "assets/plugins/jquery-file-upload/js/jquery.fileupload-validate.js",
            "assets/plugins/jquery-file-upload/js/jquery.fileupload-ui.js",
            "assets/plugins/gritter/js/jquery.gritter.js",
            "assets/js/award-training.js",
			"assets/plugins/parsley/dist/parsley.js"
		);


	var $dbname_default = "saleaward_";
	var $otherdb ;
	var $myforge ;
	public function __construct(){
		parent::__construct();
		
		$this->config->load('config_table');
		$this->load->dbutil();
		$this->load->dbforge();
		$this->load->model('training_model');
		$this->load->model('telephone_model');

		$this->load->model('adjust_model');
		$this->load->model('dealers_model');
		$this->load->model('employee_model');
		$this->load->library('form_validation');
		$this->load->library('upload');
		// $this->load->dbforge($otherdb);
	}


	public function createDatabase(){
		
		// var_dump($_POST);
		$name = $_POST['year'];
		$arr = array();
		$database_name = $this->dbname_default . $name ;
		if($name){
			if (!$this->dbutil->database_exists($database_name))
			{
				if ($this->dbforge->create_database($database_name))
				{

					$arr = array(
						'year'=> $name ,
						'db_name'=> $database_name ,
						'created_at'=> date('Y-m-d H:i:s')
					);
					$this->adjust_model->set_year($arr);
					$this->create_table($database_name);
					$arr = array("status" => true , "message" =>"สร้างฐานข้อมูลเรียบร้อยแล้ว!") ;
				}
				else{
					$arr = array("status" => false , "message" =>"เกิดเหตุขัดข้องบางประการ ไม่สามารถสร้างฐานข้อมูลได้") ;
				}
			}
			else{
				$arr = array("status" => false , "message" =>"มีฐานข้อมูลนี้แล้ว ไม่สามารถสร้างฐานข้อมูลได้") ;
			}
		}
		else
		{
			$arr = array("status" => false , "message" =>"กรุณาระบุปีฐานข้อมูลที่สร้าง") ;
		}
		
		$json =json_encode($arr);


		echo  $json ; 

	}

	public function create_table($database_new_name){
		$otherdb =$this->load->database("otherdb", TRUE);
		$otherdb->db_select($database_new_name);
		$this->load->dbforge($otherdb);

	
	    $table_settings = $this->config->item('table_settings');
		foreach($table_settings as $tablename => $item){
			$this->dbforge->add_field('id', true );
			$this->dbforge->add_field($item['fields']);

			if($item['keys']){

				foreach($item['keys'] as $key => $keys){
					
					if(is_array($keys)){
						$this->dbforge->add_key($keys);
					}
					else
					{
						$this->dbforge->add_key($keys);
					}

					
					unset($keys);
				}
			}
			$attributes = array('ENGINE' => 'InnoDB');
			$this->dbforge->create_table( $tablename , true , $attributes);
		}

	}

	public function delDatabase(){
		$id = $_POST['id'];
		$password = $_POST['password'];
		$arr = array();

		 $user = $this->session->userdata("logged_in");
		 $count_user =$this->user_model->get_UserPassDuplicate($user['id'],$password);
		 
		//  var_dump($count_user);
		$database_name = '';
		if($count_user)
		{
			$result =$this->adjust_model->get_year_by_id($id);
			if($result)
			{
				$database_name = $result['db_name'];
			}

			if ($this->dbutil->database_exists($database_name))
			{
				$result =$this->adjust_model->del_year($id);
				if ($this->dbforge->drop_database($database_name))
				{
					$arr = array("status"=> true , "message"=> "ลบฐานข้อมูลเรียบร้อยแล้ว");
				}
			}
			else
			{
				$arr = array("status"=> false , "message"=> "ไม่มีฐานข้อมูลนี้");
			}

		}
		else
		{	
			$arr = array("status"=> false , "message"=> "รหัสผ่านไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง");
		}
		
		echo json_encode($arr);

	}
	
	public function index()
	{
		
		$this->middle = 'dealer/index';
		$this->title = 'ฐานข้อมูลผู้แทนจำหน่าย';
	
		$this->data['breadcrumb'] = array(
			array('name'=>'การปรับปรุงฐานข้อมูล' , 'link' => BASE_URL('dealer') , 'active' => false ) ,
			array('name'=>'ฐานข้อมูลผู้แทนจำหน่าย' , 'link' => BASE_URL("dealer") , 'active' => true )
		); 
        $this->js = array('FormPlugins.init();', 'TableManageButtons.init();');
		$this->view();
	}

	public function add_competition()
	{
		
		$this->middle = 'adjust/add_competition';
		$this->title = 'สร้างการแข่งขันประจำปี';

		$this->data['years'] = $this->adjust_model->get_year();
	
		$this->data['breadcrumb'] = array(
			array('name'=>'การปรับปรุงฐานข้อมูล' , 'link' => BASE_URL('dealer') , 'active' => false ) ,
			array('name'=>'สร้างการแข่งขันประจำปี' , 'link' => BASE_URL("dealer") , 'active' => true )
		); 
        $this->js = array('Adjust.init();');
		$this->view();
	}

	public function import($type,$year=null)
	{
		
		$this->middle = 'tranning/import';

		$this->data['years'] = $this->adjust_model->get_year();
		$this->data['type'] = $type;
		$this->data['year'] = $year;

		switch ($type) {
			case 1:
				$this->title = 'นำเข้าข้อมูลผู้ฝึกสอน';
				$this->titleTable = 'ข้อมูลผู้ฝึกสอนประจำปี ' . $year;
				$this->data['url']= base_url(). 'training/get_datatable_training/' . $year;
				$this->data['columns']= array('ลำดับ','รหัสสาขา','รหัสตัวแทน','รหัสผู้ฝึกสอน','ชื่อ','นามสกุล','ปีที่สมัคร','อัพเดท');
				break;
			case 2:
				$this->title = 'นำเข้าข้อมูลการสำรวจความรู้ของพนักงานขายโดยทางโทรศัพท์';
				$this->titleTable = 'ข้อมูลการสำรวจความรู้ของพนักงานขายโดยทางโทรศัพท์ประจำปี ' . $year;
				$this->data['url']= base_url(). 'training/get_datatable_telephone_survey/' . $year;
				$this->data['columns']= array('ลำดับ','รหัสตัวแทน','รหัสสาขา','ไตรมาส','ปี','ระดับ','อัพเดท');
				break;
			case 3:
				$this->title = 'นำเข้าข้อมูลทดสอบความรู้ทางระบบ E-Exam';
				$this->titleTable = 'ข้อมูลทดสอบความรู้ทางระบบ E-Exam ปี ' . $year;
				$this->data['url']= base_url(). 'training/get_datatable_exam/' . $year;
				$this->data['columns']= array('ลำดับ','รหัสพนักงาน','ชื่อแบบทดสอบ','คะแนน','ปี','อัพเดท');
				break;
			
			default:
				# code...
				break;
		}
	
		$this->data['breadcrumb'] = array(
			array('name'=>'การปรับปรุงฐานข้อมูล' , 'link' => BASE_URL('dealer') , 'active' => false ) ,
			array('name'=>'นำเข้าข้อมูล' , 'link' => BASE_URL("dealer") , 'active' => true )
		); 
        $this->js = array('training.init();');
		$this->view();
	}

	public function do_upload(){

        $config['upload_path']          = './upload';
        $config['allowed_types']        = 'xls|xlsx|XLSX|XLSX|csv';
        $config['encrypt_name'] = TRUE;

		
         $config['max_size']             = 20480;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        // $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload('files'))
        {
           $data = array('error' => $this->upload->display_errors());
        	//var_dump($data);
            // $this->load->view('upload_form', $error);
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $path =$data['upload_data']['full_path'];
            // $result = $this->importData($path);
            // $html=$this->html_user($result);
            $data = array("upload"=>$data , 'result' => $path , 'file_name' => $data['upload_data']['file_name']) ;

           
            // $this->load->view('upload_success', $data);
        }


        echo json_encode($data);

    }

	public function get_data_excel($inputFileName){

		// $inputFileName = "media/Active SM.xlsx";  
		$inputFileType = PHPExcel_IOFactory::identify($inputFileName);  
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);  
		$objReader->setReadDataOnly(true);  
		$objPHPExcel = $objReader->load($inputFileName);  

		$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
		$highestRow = $objWorksheet->getHighestRow();
		$highestColumn = $objWorksheet->getHighestColumn();

		$headingsArray = $objWorksheet->rangeToArray('A1:'.$highestColumn.'1',null, true, true, true);
		$headingsArray = $headingsArray[1];

		$r = -1;
		$namedDataArray = array();
		for ($row = 2; $row <= $highestRow; ++$row) {
			$dataRow = $objWorksheet->rangeToArray('A'.$row.':'.$highestColumn.$row,null, true, true, true);
		    //  var_dump($dataRow);

			if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
				++$r;
				foreach($headingsArray as $columnKey => $columnHeading) {
					$namedDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
				}
			}
		}

		return  $namedDataArray ;

	}

	public function update_data(){
		$fileName  = $_POST['filename'];
		$year  = $_POST['year'];
		$type  = $_POST['type'];

		$database_new_name = $this->dbname_default. $year ;

		$otherdb =$this->load->database("otherdb", TRUE);
		$otherdb->db_select($database_new_name);
		$this->otherdb =  $otherdb;

		switch ($type) {
			case 1:
				//delear
				$this->training_data($fileName);
				break;
			case 2:
				$this->telephone_survey_data($fileName);
				break;
			case 3:
				$this->Exam_Data($fileName);
				break;
			
			
		}


		$data  = array('status'=> true , 'message'=> 'ประมวลผลสำเร็จแล้ว');
		echo json_encode($data);

		$path ="upload/" . $fileName ;
		if(is_file($path))
		unlink($path); // delete file
	}

	private function dateExcel2date($data){
		$result_data = '';
		// var_dump($data['work_startdate']);
		if($data){
			$result_data =  date("Y-m-d", PHPExcel_Shared_Date::ExcelToPHP($data)); 
		}

		return $result_data;
	}


	public function training_data($FileName){

		$inputFileName = "upload/" . $FileName ;  
		$result = $this->get_data_excel($inputFileName);

		$first_arr=$result[0];	
		if(!isset($first_arr['Dlr_Code']) && !isset($first_arr['Br_Code']) && !isset($first_arr['Tn_Code']) ){
			$arr = array("status"=>false, "message"=>"คอลั่มไฟล์ที่อัพโหลดไม่ถูกต้อง " );
			echo json_encode($arr);
			exit();
		}

		$i = 0;
		foreach ($result as $data) {
				$i++;
				
			    $day = date("Y-m-d H:i:s");
			   
			   if(!isset($data['Tn_Code'])) {
				   $data['Tn_Code']= '';
			   }
			   $year = '';
			   if(isset($data['ApplyForY'])){
					$year = $data['ApplyForY']-543 ; ;
			    }
				
				$data_array  = array(
					'dealer_code'=> $data['Dlr_Code'],
					'dealer_name'=> $data['DLR_NAME'],
					'branch_code'=> $data['Br_Code'],
					'trainer_id'=> $data['T_ID'],
					'trainer_code'=> $data['Tn_Code'],
					'firstname'=> $data['T_Name'],
					'lastname'=> $data['Surname'],
					'apply'=> $data['Apply'] ,
					'apply_year'=> $year ,
					'rindex'=> $data['Rindex'],
					'created_at'=> $day ,
				);

				$query = $this->otherdb
					->where('trainer_code',$data['Tn_Code'])
					->get('tb_trainer');

				if ($query->num_rows() > 0) {
					$this->otherdb
					->where('trainer_code',$data['Tn_Code'])
					->update('tb_trainer', $data_array);
				}
				else
				{
					$this->otherdb->insert('tb_trainer', $data_array);
					$id = $this->otherdb->insert_id();
				}
		}
	}
	public function telephone_survey_data($FileName){

		$inputFileName = "upload/" . $FileName ;  
		$result = $this->get_data_excel($inputFileName);

		$first_arr=$result[0];	
		if(!isset($first_arr['Dlr_Code']) && !isset($first_arr['Br_Code']) && !isset($first_arr['Quarter']) ){
			$arr = array("status"=>false, "message"=>"คอลั่มไฟล์ที่อัพโหลดไม่ถูกต้อง " );
			echo json_encode($arr);
			exit();
		}

		$i = 0;
		foreach ($result as $data) {
				$i++;
				
			    $day = date("Y-m-d H:i:s");
			   
			   
			   $year = '';
			   $Quarter = '';
			   if(isset($data['Quarter'])){
					$arr= explode("-",$data['Quarter']);
					$Quarter = (int)$arr[0];
					$year = "20" .$arr[1];
			    }
				
				$data_array  = array(
					'dealer_code'=> $data['Dlr_Code'],
					'branch_code'=> $data['Br_Code'],
					'sm_sex'=> $data['SM_Sex'],
					'question_num'=> $data['Question_Num'],
					'corrected_ans'=> $data['Corrected_Ans'],
					'reason_fail'=> $data['Reason_Fail'],
					'rating_answer'=> $data['Rating_Answer'],
					'interview_date'=> $data['Interview_Date'],
					'pic'=> $data['PIC'],
					'quarter'=> $Quarter,
					'year'=> $year ,
					'index_code'=> $data['Index_Code'],
					'created_at'=> $day ,
				);

				$query = $this->otherdb
					->where('index_code',$data['Index_Code'])
					->get('tb_telephone_survey');

				if ($query->num_rows() > 0) {
					$this->otherdb
					->where('index_code',$data['Index_Code'])
					->update('tb_telephone_survey', $data_array);
				}
				else
				{
					$this->otherdb->insert('tb_telephone_survey', $data_array);
					$id = $this->otherdb->insert_id();
				}
		}
	}

	public function ExamData($FileName){

		$inputFileName = "upload/" . $FileName ;  
		$result = $this->get_data_excel($inputFileName);
		$first_arr=$result[0];	
		if(!isset($first_arr['Exam_Code']) && !isset($first_arr['SM_Code']) && !isset($first_arr['Score']) ){
			$arr = array("status"=>false, "message"=>"คอลั่มไฟล์ที่อัพโหลดไม่ถูกต้อง " );
			echo json_encode($arr);
			exit();
		}
		$i = 0;
		foreach ($result as $data) {
				$i++;
				
			    $day = date("Y-m-d H:i:s");
			    $codeIndex ="";
			    $year ="";
			    if(isset($data['Exam_Code'])){
					$code_arr= explode("-",$data['Exam_Code']);
					$codeIndex = (int)$code_arr[0];
					$year = "20" .$code_arr[1];
			    }
				
				$data_array  = array(
					'salesman_code'=> $data['SM_Code'],
					'exam_name'=> $data['E_Name'],
					'score'=> $data['Score'],
					'exam_code'=> $codeIndex ,
					'exam_year'=> $year ,
					'exam_score'=> $data['Score'],
					'created_at'=> $day ,
				);

				$query = $this->otherdb
					->where('salesman_code',$data['SM_Code'])
					->where('exam_code',$codeIndex )
					->where('exam_year',$year )
					->get('tb_exam');

				if ($query->num_rows() > 0) {
					$this->otherdb->where('salesman_code', $data['SM_Code']);
					$this->otherdb->where('exam_code',$codeIndex ) ;
					$this->otherdb->where('exam_year', $year ) ;
					$this->otherdb->update('tb_exam', $data_array);
				}
				else
				{
					$this->otherdb->insert('tb_exam', $data_array);
					$id = $this->otherdb->insert_id();
				}

				
		}

	}


	//display table sales
	public function get_datatable_training($year){
		$database_new_name = $this->dbname_default. $year ;

		$result = $this->training_model->get_datatables($database_new_name);
		$data = array();
        $no = $_POST['start'];
        foreach ($result as $customers) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $customers->dealer_code;
            $row[] = $customers->dealer_name;
            $row[] = $customers->trainer_code;
            $row[] = $customers->firstname;
            $row[] = $customers->lastname;
            $row[] = $customers->apply_year;
            $row[] = date('d-m-Y' , strtotime($customers->created_at));
 
            $data[] = $row;
        }
 
		$json= array("draw" => $_POST['draw'] , 
					"recordsTotal" => $this->training_model->count_all($database_new_name) ,
					'recordsFiltered' => $this->training_model->count_filtered($database_new_name),
					"data"=> $data );
		echo json_encode($json);
	}
	public function get_datatable_telephone_survey($year) {

		$database_new_name = $this->dbname_default. $year ;

		$result = $this->telephone_model->get_datatables($database_new_name);
		$data = array();
        $no = $_POST['start'];
        foreach ($result as $customers) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $customers->dealer_code;
            $row[] = $customers->branch_code;
            $row[] = $customers->quarter;
            $row[] = $customers->year;
            $row[] = $customers->rating_answer;
            $row[] = date('d-m-Y' , strtotime($customers->created_at));
 
            $data[] = $row;
        }
 
		$json= array("draw" => $_POST['draw'] , 
					"recordsTotal" => $this->telephone_model->count_all($database_new_name) ,
					'recordsFiltered' => $this->telephone_model->count_filtered($database_new_name),
					"data"=> $data );
		echo json_encode($json);
	}

	public function get_datatable_exam($year){
		$database_new_name = $this->dbname_default. $year ;
		$this->load->model('exam_model');

		$result = $this->exam_model->get_datatables($database_new_name);
		$data = array();
        $no = $_POST['start'];
        foreach ($result as $item) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $item->salesman_code;
            $row[] = $item->exam_name;
            $row[] = $item->exam_score;
            $row[] = $item->exam_year;
            $row[] = date('d-m-Y' , strtotime($item->created_at));
            $data[] = $row;
        }
 
		$json= array("draw" => $_POST['draw'] , 
					"recordsTotal" => $this->exam_model->count_all($database_new_name) ,
					'recordsFiltered' => $this->exam_model->count_filtered($database_new_name),
					"data"=> $data );
		echo json_encode($json);
	}
	
}
