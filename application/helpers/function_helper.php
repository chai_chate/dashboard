<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('condition'))
{
    function condition($operation,$percentage,$point1,$point2)
    {
        $condition = false ;
        switch ($operation) {
            case 1 :
                if($percentage > $point1){
                    $condition = true ;
                } 
                break;
            case 2 :
                if($percentage >= $point1){
                    $condition = true ;
                } 
                break;
            case 3 :
                if($percentage < $point1){
                    $condition = true ;
                } 
                break;
            case 4 :
                if($percentage <= $point1){
                    $condition = true ;
                } 
                break;
            case 5 :
                if($percentage = $point1){
                    $condition = true ;
                } 
                break;
            case 6 :
                if($percentage >= $point1 && $percentage <= $point2){
                    $condition = true ;
                } 
                break;

            default:
                $condition = false ;
                 break;
         }

         return $condition ;

    }   
}