<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Schedule_model extends CI_Model {

        public $title;
        public $content;
        public $date;
        

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

    
        public function get_schedule()
        {
                $query = $this->db->get('tb_schedule');
                return $query->result();
        }

        public function get_schedule_last()
        {
                $day= date("Y-m-d");
                $this->db->limit(1); 
                $this->db->order_by('day', 'ASC');
                $this->db->WHERE("day >", $day );
                $query = $this->db->get('tb_schedule');
                return $query->row();
        }


        public function get_schedulebyDate()
        {
                $day= date("Y-m-d");
                $this->db->WHERE("day", $day );
                $query = $this->db->get('tb_schedule');
                $rowcount = $query->num_rows();

                return $rowcount;
        }

        public function get_schedulebyId($id)
        {
                $this->db->WHERE("id", $id );
                $query = $this->db->get('tb_schedule');
                return $query->row();
        }


        public function save_schedule($arr)
        {
            $this->db->insert('tb_schedule', $arr);
            return $this->db->insert_id();

        }
        public function update_schedule_number($arr , $id)
        {
            $this->db->WHERE("id", $id );
            $this->db->update('tb_schedule', $arr);
            return $this->db->insert_id();

        }

        
        public function update_status($status,$id)
        {

            if($status == 1)
            {
                $arr = array(
                    "status"=> '2'
                );
                $this->db->where('status', '1');
                $result = $this->db->update('tb_schedule', $arr);
            }



            $arr = array(
                "status"=>$status
            );
            $this->db->where('id', $id);
            $result = $this->db->update('tb_schedule', $arr);
            return $result;

        }

        public function update_entry()
        {
                $this->title    = $_POST['title'];
                $this->content  = $_POST['content'];
                $this->date     = time();

                $this->db->update('entries', $this, array('id' => $_POST['id']));
        }

}
?>