<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dealers_model extends CI_Model {

    var $table = 'tb_dealers';
    var $column_order = array(null, 'dealer_code','dealer_name','group_id','group','region_code','created_at'); //set column field database for datatable orderable
    var $column_search = array('dealer_code','dealer_name','group'); //set column field database for datatable searchable 
    var $order = array('id' => 'asc'); // default order 
    var $other ;

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        
        $this->other =$this->load->database("otherdb", TRUE);
    }

    public function get_dealers_by_id($id)
    {
        if($id != FALSE) {
            $query = $this->db->get_where('tb_dealers', array('id' => $id));
            return $query->row_array();
        }
        else {
            return FALSE;
        }
    }

    public function get_dealers($where = array())
    {
        $query = $this->db->get_where('tb_dealers', $where );
        return $query->result();
    }

    public function get_group($where=array())
    {
        $query = $this->db->get_where('tb_group', $where );
        return $query->result();
    }

    public function get_group_by_id($id)
    {
        if($id != FALSE) {
            $query = $this->db->get_where('tb_group', array('id' => $id));
            return $query->row_array();
        }
        else {
            return FALSE;
        }
    }

    public function get_Count()
    {
            $query = $this->other->get('tb_dealers');
            $rowcount = $query->num_rows();

            return $rowcount;
    }

    public function _change_database($database_new_name){
        $this->other->db_select($database_new_name);
    }

    private function _get_datatables_query()
    {
         
        $this->other->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->other->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->other->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->other->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->other->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->other->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->other->order_by(key($order), $order[key($order)]);
        }
    } 
    function get_datatables($database)
    {
        $this->_change_database($database);

        $this->_get_datatables_query($database);
        if($_POST['length'] != -1)
        $this->other->limit($_POST['length'], $_POST['start']);
        $query = $this->other->get();
        return $query->result();
    }
 
    function count_filtered($database)
    {
        $this->_get_datatables_query();
        $query = $this->other->get();
        return $query->num_rows();
    }
 
    public function count_all($database)
    {
        $this->other->from($this->table);
        return $this->other->count_all_results();
    }

    

}
?>