<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee_model extends CI_Model {

        public $title;
        public $content;
        public $date;
        var $table = 'tb_employee';
        var $column_order = array(null, 'firstname','lastname','position','sex','branch_code','dealer_code','start_date'); //set column field database for datatable orderable
        var $column_search = array('firstname','lastname','branch_code','dealer_code'); //set column field database for datatable searchable 
        var $order = array('id' => 'asc'); // default order 
        var $other ;
        

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
                $this->other =$this->load->database("otherdb", TRUE);

        }

        public function get_employee_id($id)
        {
                $query = $this->db->where("id", $id )
                         ->get('tb_employee');
                return $query->row();
        }

        public function get_employee($search)
        {

                // var_dump($search);
                foreach($search as $key => $value) {
                      if(!empty($value)) {  
                        if($key == 0) {
                                $this->other->WHERE($key, $value);
                        } else {
                                $this->other->or_like($key, $value);
                        }
                      }
                }
                $query = $this->other->get('tb_employee');
                // echo $this->db->last_query();
                return $query->result();
        }
        public function get_employee_type($type)
        {
                $this->db->WHERE("type", $type);
                $query = $this->other->get('tb_employee');
                // echo $this->db->last_query();
                return $query->result();
        }
        public function get_employee_typeCount($type)
        {
                $this->other->WHERE("ddms", $type);
                $query = $this->other->get('tb_employee');
                // echo $this->db->last_query();
                $rowcount = $query->num_rows();

                return $rowcount;
        }

        public function get_employee_level()
        {
                $query = $this->db->get('tb_employee_level');
                return $query->result();

        }
        public function get_employee_level_id($id=FALSE)
        {
               if($id != FALSE) {
                        $query = $this->db->get_where('tb_employee_level', array('id' => $id));
                        return $query->row_array();
                }
                else {
                        return FALSE;
                }
        }


        public function set_employee($arr)
        {
            $this->other->insert('tb_employee', $arr);
            return $this->other->insert_id();

        }
        public function update_employee($arr,$id)
        {
            $this->other->where('id', $id);
            $this->other->update('tb_employee', $arr);
            return $id;

        }

        public function update_entry()
        {
                $this->title    = $_POST['title'];
                $this->content  = $_POST['content'];
                $this->date     = time();

                $this->db->update('entries', $this, array('id' => $_POST['id']));
        }

   public function _change_database($database_new_name){
        $this->other->db_select($database_new_name);
    }

    private function _get_datatables_query()
    {
         
        $this->other->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->other->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->other->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->other->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->other->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->other->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->other->order_by(key($order), $order[key($order)]);
        }
    } 
    function get_datatables($database)
    {
        $this->_change_database($database);

        $this->_get_datatables_query($database);
        if($_POST['length'] != -1)
        $this->other->limit($_POST['length'], $_POST['start']);
        $query = $this->other->get();
        return $query->result();
    }
 
    function count_filtered($database)
    {
        $this->_get_datatables_query();
        $query = $this->other->get();
        return $query->num_rows();
    }
 
    public function count_all($database)
    {
        $this->other->from($this->table);
        return $this->other->count_all_results();
    }

}
?>