<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Set_model extends CI_Model {

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_set_id($id)
    {
        $query = $this->db->where("id", $id )
                ->get('tb_set');
        return $query->row();
    }
    public function get_set($where=array(),$group_by=null)
    {
        if($group_by){
            $this->db->group_by($group_by); 
        }
        $query = $this->db->get_where('tb_set', $where );

        return $query->result();
    }
    public function delete_set($id)
    {
        $this->db->where("id" , $id ) ;
       return $this->db->delete('tb_set'); 
    }

    public function get_allset($search=array())
    {
        foreach($search as $key => $value) {
            if(!empty($value)) {  
                if($key == 0) {
                        $this->db->WHERE($key, $value);
                } else {
                        $this->db->or_like($key, $value);
                }
            }
        }
        $query = $this->db->get('tb_set');
        // echo $this->db->last_query();
        return $query->result();
    }

}
?>