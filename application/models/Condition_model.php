<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Condition_model extends CI_Model {

    public $title;
    public $content;
    public $date;
    
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function insert_set($data= array())
    {
       $this->db->insert('tb_set', $data);
       return $this->db->insert_id();
    }

    public function insert_month_score($data= array())
    {
       $this->db->insert('tb_month_score', $data);
       return $this->db->insert_id();
    }

    public function delete_month_score($set,$unit_start,$unit_end)
    {
        $this->db->where("set_id" , $set ) ;
        $this->db->where("units >" , $unit_start );
        $this->db->or_where("units <" , $unit_end );
       return $this->db->delete('tb_month_score'); 
    }
    public function delete_month_by_set($set)
    {
        $this->db->where("set_id" , $set ) ;
       return $this->db->delete('tb_month_score'); 
    }

    public function insert_condition_score($data= array())
    {
       $this->db->insert('tb_condition', $data);
       return $this->db->insert_id();
    }
    public function update_condition_score($where=array(),$data= array())
    {
       $this->db->where($where); 
       return $this->db->update('tb_condition', $data);
    }
    public function get_condition_count($where=array())
    {
        $query = $this->db->get_where('tb_condition', $where );
        $rowcount = $query->num_rows();
        // echo $this->db->last_query();
        return $rowcount;
    }
    public function delete_condition_score($set)
    {
       $this->db->where("set_id" , $set ) ;
       return $this->db->delete('tb_condition'); 
    }


    public function insert_avanza_score($data= array())
    {
       $this->db->insert('tb_avanza_score', $data);
       return $this->db->insert_id();
    }
    public function delete_avanza_score($set)
    {
       $this->db->where("set_id" , $set ) ;
       return $this->db->delete('tb_avanza_score'); 
    }



    public function insert_interview_score($data= array())
    {
       $this->db->insert('tb_interview_rating', $data);
       return $this->db->insert_id();
    }
    public function delete_interview_score($set)
    {
        $this->db->where("set_id" , $set ) ;
       return $this->db->delete('tb_interview_rating'); 
    }
    public function update_interview_score($where=array(),$data= array())
    {
       $this->db->where($where); 
       return $this->db->update('tb_interview_rating', $data);
    }
    public function get_interview_count($where=array())
    {
        $this->db->where($where); 
        $query = $this->db->get('tb_interview_rating');
        $rowcount = $query->num_rows();

        return $rowcount;
    }




    //ssmi
    public function insert_ssmi_score($data= array())
    {
       $this->db->insert('tb_ssmi_rating', $data);
       return $this->db->insert_id();
    }
    public function delete_ssmi_score($set)
    {
        $this->db->where("set_id" , $set ) ;
       return $this->db->delete('tb_ssmi_rating'); 
    }
    public function update_ssmi_score($where=array(),$data= array())
    {
       $this->db->where($where); 
       return $this->db->update('tb_ssmi_rating', $data);
    }
    public function get_ssmi_count($where=array())
    {
        $this->db->where($where); 
        $query = $this->db->get('tb_ssmi_rating');
        $rowcount = $query->num_rows();

        return $rowcount;
    }

    // public function get_award_type($where=array())
    // {
    //     $query = $this->db->get_where('tb_award_type', $where );
    //     return $query->result();
    // }

}
?>