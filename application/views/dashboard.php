	<div class="row">
			    <!-- begin col-3 -->
			    <div class="col-md-3 col-sm-6">
			        <div class="widget widget-stats bg-blue-grey">
			            <div class="stats-icon stats-icon-lg"><i class="material-icons">people</i></div>
			            <div class="stats-title">จำนวนพนักงานขาย</div>
			            <div class="stats-number"><?php if(isset($count_seller)) echo $count_seller; else echo "0";?></div>
			            <div class="stats-progress progress">
                            <div class="progress-bar" style="width: 70.1%;"></div>
                        </div>
                        <!--<div class="stats-desc">Better than last week (70.1%)</div>-->
			        </div>
			    </div>
			    <!-- end col-3 -->
			    <!-- begin col-3 -->
			    <div class="col-md-3 col-sm-6">
			        <div class="widget widget-stats bg-blue">
			            <div class="stats-icon stats-icon-lg"><i class="material-icons">people</i></div>
			            <div class="stats-title">จำนวนผู้ฝึกสอน</div>
			            <div class="stats-number"><?php if(isset($count_trainer)) echo $count_trainer; else echo "0"; ?></div>
			            <div class="stats-progress progress">
                            <div class="progress-bar" style="width: 40.5%;"></div>
                        </div>
                        <!--<div class="stats-desc">Better than last week (40.5%)</div>-->
			        </div>
			    </div>
			    <!-- end col-3 -->
			    <!-- begin col-3 -->
			    <div class="col-md-3 col-sm-6">
			        <div class="widget widget-stats bg-cyan">
			            <div class="stats-icon stats-icon-lg"><i class="material-icons">people_outline</i></div>
			            <div class="stats-title">จำนวนผู้จัดการขาย</div>
			            <div class="stats-number"><?php if(isset($count_mrg)) echo $count_mrg; else echo "0"; ?></div>
			            <div class="stats-progress progress">
                            <div class="progress-bar" style="width: 76.3%;"></div>
                        </div>
                        <!--<div class="stats-desc">Better than last week (76.3%)</div>-->
			        </div>
			    </div>
			    <!-- end col-3 -->
			    <!-- begin col-3 -->
			    <div class="col-md-3 col-sm-6">
			        <div class="widget widget-stats bg-deep-purple">
			            <div class="stats-icon stats-icon-lg"><i class="material-icons">account_balance</i></div>
			            <div class="stats-title">จำนวนผู้แทนจำหน่าย</div>
			            <div class="stats-number"><?php if(isset($count_deales)) echo $count_deales; else echo "0"; ?></div>
			            <div class="stats-progress progress">
                            <div class="progress-bar" style="width: 54.9%;"></div>
                        </div>
                        <!--<div class="stats-desc">Better than last week (54.9%)</div>-->
			        </div>
			    </div>
			    <!-- end col-3 -->
			</div>
			<!-- end row -->
			
			
			