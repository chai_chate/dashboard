
<!-- begin #sidebar -->
		<div id="sidebar" class="sidebar">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
					    <a href="#" data-toggle="nav-profile">
                            <!--<div class="image">
                                <img src="<?php echo $user['avatar'];?>" alt="" />
                            </div>-->
                            <div class="info">
                              <b class="caret pull-right"></b>
							    <?php if(isset($user)) echo $user['name'];?>
                                <small>TOYOTA ADMIN</small>
                            </div>
						</a>
					</li>
                    <li>
                        <ul class="nav nav-profile">
                            <li><a href="#"><i class="material-icons">settings</i> Settings</a></li>
                            <li><a href="#"><i class="material-icons">mode_edit</i> Send Feedback</a></li>
                            <li><a href="#"><i class="material-icons">help</i> Helps</a></li>
                        </ul>
                    </li>
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<ul  class="nav">
					<li class="nav-header">เมนู</li>
					<li <?php if(strtolower($this->uri->segment(1))=="dashboard") echo 'class="active"';?> >
						<a href="<?php echo base_url()?>dashboard">
						<i class="material-icons">home</i>
						<span>Dashboard</span></a>
					</li>
					<li <?php if(strtolower($this->uri->segment(1))=="competition" || strtolower($this->uri->segment(1))=="processor" || strtolower($this->uri->segment(1))=="report" ) echo 'class="has-sub active"';?> class="has-sub">
						<a href="javascript:;">
						    <b class="caret pull-right"></b>
							<i class="material-icons">content_paste</i>
						    <span>ผลการแข่งขัน</span> 
						</a>
						<ul class="sub-menu">
							<li><a href="<?php echo base_url('Processor/');?>">- ประมวลผลการแข่งขัน</a></li>	
							<!--<li><a href="<?php echo base_url('competition/2556');?>">-ปีการแข่งขัน 2556</a></li>
							<li><a href="<?php echo base_url('competition/2557');?>">-ปีการแข่งขัน 2557</a></li>
							<li><a href="<?php echo base_url('competition/2558');?>">-ปีการแข่งขัน 2558</a></li>-->
							<li><a href="<?php echo base_url('competition/2016');?>">- ผลการแข่งขัน</a></li>	
							<!--<li><a href="<?php echo base_url('competition/2560');?>">-ปีการแข่งขัน 2560</a></li>	-->
							<li><a href='<?php echo base_url('report/nation');?>'>- พิมพ์รายงานผลการแข่งขัน</a></li>

						</ul>
					</li>
					<li <?php if(strtolower($this->uri->segment(1))=="condition") echo 'class="has-sub active"';?> class="has-sub">
						<a href="javascript:;">
						    <b class="caret pull-right"></b>
							<i class="material-icons">import_contacts</i>
						    <span>กติกาการแข่งขัน</span> 
						</a>
						<ul class="sub-menu">
							<li><a href="<?php echo base_url('condition'); ?>">- การกำหนดกติกาการแข่งขัน</a></li>
							<li><a href="<?php echo base_url('condition/setlist'); ?>">- เลือกกติกาการแข่งขัน</a></li>
							<!--<li><a href="<?php echo base_url('condition/updatRules/'); ?>">- การปรับปรุงกติกาการแข่งขัน</a></li>-->
						</ul>
					</li>
					<li <?php if(strtolower($this->uri->segment(1))=="adjust") echo 'class="has-sub active"';?> class="has-sub">
						<a href="javascript:;">
						    <b class="caret pull-right"></b>
						    <i class="material-icons">chrome_reader_mode</i>
						    <span>การปรับปรุงข้อมูล</span> 
						</a>
						<ul class="sub-menu">
							<li><a href='<?php echo base_url('adjust/add_competition/');?>'>- สร้างการแข่งขันประจำปี</a></li>
							<li><a href='<?php echo base_url('adjust/import/1/2016');?>'>- นำเข้าข้อมูลการแข่งขัน</a></li>
							<!--<li><a href='<?php echo base_url('training/import/1/2016');?>'>- นำเข้าข้อมูลการแข่งขันผู้ฝึกสอนยอดเยี่ยม</a></li>
							<li><a href='<?php echo base_url('adjust/import/1/2016');?>'>- นำเข้าข้อมูลการแข่งขันผู้จัดการยอดเยี่ยม</a></li>
							-->
						</ul>
					</li>
					<!--<li class="has-sub">
						<a href="javascript:;">
						    <b class="caret pull-right"></b>
						    <i class="material-icons">group_work</i>
						    <span>รางวัลอื่นๆ</span> 
						</a>
						<ul class="sub-menu">
							<li><a href="#">- President's Award</a></li>
							<li><a href="#">- Best of the Best</a></li>
							<li><a href="#">- Best of sales</a></li>
							<li><a href="#">- Best After Sales</a></li>
							<li><a href="#">- Best After Sales</a></li>
							<li><a href="#">- Best CRL</a></li>
						</ul>
					</li>-->
					<!--<li class="has-sub">
						<a href="javascript:;">
						    <b class="caret pull-right"></b>
						    <i class="material-icons">report</i>
						    <span>การรายงานผล</span> 
						</a>
						<ul class="sub-menu">
							
							<li><a href='<?php echo base_url('report/nation');?>'>- ตามประเภท</a></li>
						</ul>
					</li>-->
			        <!-- begin sidebar minify button -->
					<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
			        <!-- end sidebar minify button -->
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		<div class="sidebar-bg"></div>
		<!-- end #sidebar -->