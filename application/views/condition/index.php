<!-- begin row -->
<div class="row">
    
    <!-- begin col-12 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                </div>
                <h4 class="panel-title"><?php echo $this->title?></h4>
            </div>
            
            <div class="panel-body">

                <div class="row">
                    <div class="col-sm-12 text-right">
                        <a href='<?php echo base_url('Condition');?>'  class="btn btn-primary m-r-5 m-b-5">สร้างเงื่อนไข</a>
                    </div>
                </div>

                <table id="data-table" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>ลำดับ</th>
                            <th>ชื่อเงื่อนไข</th>
                            <th>ประเภท</th>
                            <th>วันที่แก้ไข</th>
                            <th>สถานะ</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                          foreach ($List as $key => $item) {
                            echo '<tr class="odd gradeX">
                                <td>' . ($key+1) .'</td>
                                <td>' . $item->name .'</td>
                                <td>' . $item->level_name . '</td>
                                <td>' . $item->updated_at .'</td>
                                <td>' . $item->status .'</td>
                                <td class="text-center" width="200">
                                    <a href="' . base_url('condition/edit/').$item->id  .'" class="btn btn-info m-r-5 m-b-5">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        รายละะอียด
                                    </a>
                                    <a href="' . base_url('condition/delete_rules/').$item->id  .'" type="button" class="btn btn-danger m-r-5 m-b-5">
                                        <i class="fa fa-ban" aria-hidden="true"></i>
                                        ลบ
                                    </a>
                                </td>
                            </tr>';
                          }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- end panel -->
    </div>
    <!-- end col-10 -->
</div>
<!-- end row -->