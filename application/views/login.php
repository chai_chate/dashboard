<!-- begin login -->
<div class="login bg-grey-900 animated fadeInDown">
    <!-- begin brand -->
    <div class="login-header">
        <div class="brand text-inverse">
            <span class="logo"></span> <?php if(isset($title)) echo $title?>
            <small><?php if(isset($sub_title)) echo $sub_title?></small>
        </div>
        <div class="icon">
            <i class="material-icons">lock</i>
        </div>
    </div>
    <!-- end brand -->
    <div class="login-content">


        <?php if(validation_errors()) { ?>
         <div class="alert alert-danger fade in m-b-15">
            <strong>Warning!</strong>
            <span class="close" data-dismiss="alert">×</span>
             <?php echo validation_errors(); ?>
        </div>
        <?php } ?>

        <form action="<?php echo base_url();?>login" method="POST" class="margin-bottom-0">
            <div class="form-group m-b-20">
                <input type="text" name='username' class="form-control input-lg without-border inverse-mode" placeholder="User name" />
            </div>
            <div class="form-group m-b-20">
                <input type="password" name='password' class="form-control input-lg without-border inverse-mode" placeholder="Password" />
            </div>
            <div class="checkbox m-b-20">
                <label class="text-white">
                    <input type="checkbox" /> Remember Me
                </label>
            </div>
            <div class="login-buttons">
                <button type="submit" class="btn btn-info btn-block btn-lg">Sign me in</button>
            </div>
        </form>
    </div>
</div>
<!-- end login -->