
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <!-- begin panel -->
            <div class="panel panel-inverse" data-sortable-id="form-stuff-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title"><?php echo $this->title;?></h4>
                </div>
                <div class="panel-body">

                    <form action="<?php echo base_url('award/save_award');?>" data-parsley-validate="true"  method="POST" class="form-horizontal form-bordered">

                         <?php if(validation_errors()) { ?>
                        <div class="alert alert-danger fade in m-b-15">
                            <strong>Warning!</strong>
                            <span class="close" data-dismiss="alert">×</span>
                            <?php echo validation_errors(); ?>
                        </div>
                        <?php } ?>



                         <div class="form-group">
                            <label class="col-md-3 control-label">ວັນທີເລກອອກ</label>
                            <div class="col-md-3">
                                <select name="day" class="form-control change-employee"  data-parsley-required="true" >
                                    <?php 
                                    foreach($schedule as $key => $item)
                                    {	
                                    ?>
                                        <option <?php if(isset($current) && $current->id == $item->id) echo "selected" ;?>  value='<?php echo $item->id; ?>' >
                                            <?php echo $item->day?>
                                        </option>
                                    <?php 
                                    } 
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">ເລກຖືກລາງວັນ</label>
                            <div class="col-md-3">
                                <?php 
                                $namber= "";
                                    //  var_dump($current);
                                if(isset($current))
                                 { 
                                     $namber = $current->lottery_number;
                                 }
                                 ?>
                                <input type="text"  name="number" max="5"  value="<?php echo $namber;?>"  data-parsley-required="true" class="form-control" placeholder="ວັນທີເລກອອກ" />
                            </div>
                        </div>



                       

                      

                      
                        <div class="form-group">
                            <label class="col-md-3 control-label"></label>
                            <div class="col-md-9">
                                <button type="submit" class="btn btn-sm btn-success">ບັນທຶກ</button>
                                <button type="reset" class="btn btn-sm btn-default">ຍົກເລິກ</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end panel -->

         </div>
        <!-- end col-12 -->

    </div>
    <!-- end row -->


    <!-- begin row -->
			<div class="row">
			    <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse" data-sortable-id="table-basic-7">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">ຕາຕະລາງວັນທີ່ເລກອອກ</h4>
                        </div>
                        <div class="panel-body">
							<div class="table-responsive">
                               <table id="data-table" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>ວັນທີ່ອອກຫວຍ</th>
											<th>ເລກຖືກລາງວັນ</th>
											<th>ແກ້ໄຂ</th>
										</tr>
									</thead>
									<tbody>
									<?php 
										foreach($schedule as $key => $item)
										{
											
									?>
										<tr>
											<td><?php echo $key +1 ;?></td>
											<td width="600"><?php echo $item->day; ?></td>
											<td>
											<?php
												echo $item->lottery_number;
											?>
											</td>
											<td>
                                                <a href="<?php echo base_url('/award/page/' . $item->id );?>">ແກ້ໄຂ</a>
											</td>
											
										</tr>
										<?php } ?>


									
									</tbody>
								</table>
							</div>
						
						</div>
					</div>
                    <!-- end panel -->
			    </div>
			    <!-- end col-12 -->
			</div>
			<!-- end row -->
