var handleDataTableButtons = function() {
	"use strict";
    
    if ($('#data-table').length !== 0) {
        $('#data-table').DataTable({
            "displayLength": 30,
            dom: 'Bfrtip',
            buttons: [
                { extend: 'copy', className: 'btn-sm' },
                { extend: 'csv', className: 'btn-sm' },
                { extend: 'excel', className: 'btn-sm' },
                { extend: 'pdf', className: 'btn-sm' },
                { extend: 'print', className: 'btn-sm' }
            ],
            responsive: true
        });
    }
};
var handleSelectAward = function() {
	"use strict";
    
    if ($('.select-option').length !== 0) {

        $(".select-option").change(function(){
            var year = $(this).data("year");
            window.location = "/competition/"+ year +"/" + $(this).val() ;
        });

        $(".select-year").change(function(){
            window.location = "/competition/" + $(this).val() ;
        });


    }
};

var TableManageButtons = function () {
	"use strict";
    return {
        //main function
        init: function () {
            handleDataTableButtons();
            handleSelectAward();
        }
    };
}();