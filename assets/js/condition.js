var Path = $("body").data("baseurl");
var $panel = $("div [data-hiden-id]");
var $TypeSelect = $('.hand-select-type');

var handleConditionMonthScore = function () {
    var getTablescore = function () {
        var start = $('[data-input="start"]').val();
        var end = $('[data-input="end"]').val();
        // if ( $FormSales.parsley().isValid() ) {
        $.post("Condition/get_json_MonthScore/" + start + '/' + end + "/1", function (data) {
            $table.empty().append(data);
        });
        // }
    }
    var $table = $('[data-append="tablescore"]');
    $('[data-click="tablescore"]').click(function () {
        getTablescore();
    });

    getTablescore();


    var inputMonthScore = $("[data-month-limit]");
    var selecter = inputMonthScore.data("month-limit");
    inputMonthScore.keydown(function (e) {
        max = $(this).val() / 12;
        $.each($(selecter), function (indexInArray, valueOfElement) {
            // console.log(this);
            if ($(this).val() > max) {
                $(this).addClass("error");
            }
            else {
                $(this).removeClass("error");
            }

        });
    });




};

var handleTextinputNumbericAndMax = function () {
    $("input[type='number']").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
            // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    // $("input[max^=]").keydown(function (e) {
    //     console.log("5555");
    // });

};

var handleConditionMainForm = function () {
    var $Selectaward = $('.hand-select-award');
    //เลือกประเภทรางวัล
    $TypeSelect.change(function () {
        // var html = "";
        // $.post(Path +"condition/get_json_award/" + $(this).val() ,function(obj){
        //     $.each( obj, function( key, value ) {
        //         html += "<option>" + value.name_th + "</option>";
        //     });
        //     console.log(html);
        //     $Selectaward.empty().append(html);
        // },"json");
        index_select = $(this).val();
        $panel.hide();
        $panel.filter(function () {
            return $(this).data("hiden-id") == index_select
        }).show();
    });
    $panel.hide();
    $panel.filter(function () {
        return $(this).data("hiden-id") == 1
    }).show();


};


var handleFormSave = function () {
    var btn = $(".btn-handleFormSave");
    // var $TypeSelect = $('.hand-select-type');

    btn.click(function () {
        var index_select = $TypeSelect.val();
        var form =$panel.filter( function() { 
          return $(this).data("hiden-id") == index_select 
        }).find('form');

        $("input").removeClass("error");

        var $FormMain = $('#FormMain');
        var data = {
            'main_type_award' : $FormMain.find("[name='main_type_award']").val() ,
            'main_type_name' : $FormMain.find("[name='main_type_award'] option:selected").text() ,
            'main_set_name' : $FormMain.find("[name='main_set_name']").val()
        };
        
        
        var fullpath = Path +"condition/savedata";
        if(data.main_type_award == 1){
            fullpath = Path +"condition/savedata";
        }
        else if(data.main_type_award == 2)
        {
            fullpath = Path +"condition/saveTrainer";
        }
        else if(data.main_type_award == 3)
        {
            fullpath = Path +"condition/saveMgr";
        }

        // console.log(fullpath);


        // console.log(data);
        $.gritter.removeAll();
        // console.log(form.serialize());
        $.post(fullpath, form.serialize()+'&' + $.param(data), function(data){
                
                if(data.status == true)
                {
                    alert("บันทึกข้อมูลสำเร็จ");
                    loation.href = "condition/setlist" ;
                }

                $.each( data.message , function( key, message ) {
                   
                    $.gritter.add({
                        title: 'แจ้งเตือน',
                        text: message ,
                        sticky: true ,
                        image: 'assets/img/warning.jpg',
                        before_open: function() {
                            if($('.gritter-item-wrapper').length === 8) {
                                return false;
                            }
                        }
                    });
                });

                $.each( data.element , function( key, value ) {
                    $("input[name^='" + value +"']").addClass("error");
                });

                console.log(data.element);

        },"json");

    });
};


var handleTablePoint = function(){
    $TablePoint = $("[data-id='TablePoint']");
    $.each( $TablePoint, function( key, value ) {
        var action = $(this).data("action");

        //load
        $.post(Path +"condition/" + action ,function(data){
            $TablePoint.append(data.html);
        },"json");

        $TablePoint.on("click",".btn-handleaddPointRow",function(){
            var btn = $(this);
            btn.prop("disabled",true);
            $.post(Path +"condition/" + action ,function(data){
                $TablePoint.append(data.html);
                btn.prop("disabled", false );
            },"json");
            return false;
        });

        $TablePoint.on("click",".btn-handleDeletePointRow",function(){
            $(this).parent("td").parent("tr").remove();
            return false;
        });

        $TablePoint.on("change",".sel-handChangOption",function(){
            if($(this).val()==6)
            {
                $(this).parent("td").next("td").find(".row div").removeClass("hide");
            }
            else{
                $(this).parent("td").next("td").find(".row div").eq(1).addClass("hide");
            }
            // return false;
        });
    });

};


/* Application Controller
------------------------------------------------ */
var Condition = function () {
    "use strict";

    return {
        //main function
        init: function () {
            handleConditionMonthScore();
            handleTextinputNumbericAndMax();
            handleConditionMainForm();
            handleFormSave();
            handleTablePoint();
        },
        initSidebar: function () {

        },
    };
}();